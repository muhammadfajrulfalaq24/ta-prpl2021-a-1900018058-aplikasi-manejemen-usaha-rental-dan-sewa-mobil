<?php  
require_once("confiq.php");

if(isset($_POST['submit'])){
	$kode_transaksi = $_POST['kode_transaksi'];
	$tgl_kembali = $_POST['tgl_kembali'];
	$lama_sewa = $_POST['lama_sewa'];
	$harga_total = $_POST['harga_total'];
	$status = $_POST['status'];
	$sewa_brp = $_POST['sewa_brp'];
	$kode_as = $_POST['kode_as'];
	$kode_p = $_POST['kode_p'];

	$sql_insert = "INSERT INTO transaksi VALUES('$kode_transaksi','$tgl_kembali','$lama_sewa','$harga_total','$status','$sewa_brp','$kode_as','$kode_p')";
	mysqli_query($koneksi, $sql_insert);
	header("Location:datatransaksi.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Pesan Mobil</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<style type="text/css">
		.bodytambahtransaksi{margin: 0px; padding: 0px; background: linear-gradient(to right, green, blue); text-align: center;}
		.tabeltambahdatatransaksi{margin: auto; font-weight: bold; color: white;}
		.buttonSubmit{
			border-radius: 25px;
			background-color: green;
		}
		.buttonSubmit a{
			color: white;
		}
		h1{
			color: white;
			font-family: algerian;
			font-size: 25px;
		}
	</style>
</head>
<body class="bodytambahtransaksi">
	<br><br>
	<h1>Tambah Pemesanan Mobil</h1>
	<br><br>
	<form action="tambahdatatransaksi.php" method="POST">
		<table class="tabeltambahdatatransaksi">
			<tr>
				<td>Kode Transaksi</td>
				<td>:</td>
				<td>
					<input type="text" name="kode_transaksi">
				</td>
			</tr>
			<tr>
				<td>Tanggal Kembali</td>
				<td>:</td>
				<td>
					<input type="text" name="tgl_kembali">
				</td>
			</tr>
			<tr>
				<td>Lama Sewa</td>
				<td>:</td>
				<td>
					<input type="number" name="lama_sewa">
				</td>
			</tr>
			<tr>
				<td>Harga Total</td>
				<td>:</td>
				<td>
					<input type="number" name="harga_total">
				</td>
			</tr>
			<tr>
				<td>Status Pembayaran</td>
				<td>:</td>
				<td>
					<input type="text" name="status">
				</td>
			</tr>
			<tr>
				<td>Sewa Berapa</td>
				<td>:</td>
				<td>
					<input type="number" name="sewa_brp">
				</td>
			</tr>
			<tr>
				<td>Kode Mobil</td>
				<td>:</td>
				<td>
					<input type="text" name="kode_as">
				</td>
			</tr>
			<tr>
				<td>Kode Penyewa</td>
				<td>:</td>
				<td>
					<input type="text" name="kode_p">
				</td>
			</tr>
		</table><br><br>
		<center><button style="width: 180px; margin-top: 50px; " name="submit" type="submit" class="buttonSubmit">
		<a style="text-decoration: none; font-size: 12px; font-weight: bold;">Tambahkan Pemesanan Sewa & Mobil</a></button></center>
	</form>
</body>
</html>