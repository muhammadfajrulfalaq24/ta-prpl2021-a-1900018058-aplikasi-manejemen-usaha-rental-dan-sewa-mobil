<?php  
require_once("confiq.php");

$sql_get = "SELECT * FROM mobil";
$query_mobil = mysqli_query($koneksi, $sql_get);

$results = [];
while ($row = mysqli_fetch_assoc($query_mobil)) {
	$results[] = $row;
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Ketersediaan Mobil</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<style type="text/css">
		.tabeldatatransaksi{
			margin: auto;
			font-weight: bold;
			color: black;
			background-color: white;
			margin-top: 100px;
		}
		.datatransaksi{
			text-align: center;
			background-image: url(img/bgmobil.png);
		}
		.buttonSubmit{
			border-radius: 25px;
			background-color: green;
		}
		.buttonSubmit a{
			color: white;
		}
		table{
			border-collapse: collapse;
		}
		table td {
			border:1px solid; padding: 6px;
		}
	</style>
</head>
<body class="datatransaksi">
	<h1>Data Mobil Tersedia</h1>
	<table class="tabeldatatransaksi" border="">
		<tr>
			<td>No</td>
			<td>Kode Mobil</td>
			<td>Lama Maksimal</td>
			<td>Domisili</td>
			<td>Merk Mobil</td>
			<td>Tersedia</td>
			<td>Harga Per Hari</td>
			<td>Aksi</td>
		</tr>
		<?php  
		$no = 1;
		foreach ($results as $dataterbaca) :
		?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $dataterbaca['kode_as'] ?></td>
			<td><?= $dataterbaca['lama_max'] ?></td>
			<td><?= $dataterbaca['domisili'] ?></td>
			<td><?= $dataterbaca['merk_mobil'] ?></td>
			<td><?= $dataterbaca['tersedia'] ?></td>
			<td><?= $dataterbaca['harga_hari'] ?></td>
			<td>
				<a href="hapusdatamobil.php?kode_as=<?= $dataterbaca['kode_as']; ?>">Hapus</a>
				||
				<a href="ubahdatamobil.php?kode_as=<?= $dataterbaca['kode_as']; ?>">Edit</a>
			</td>
		</tr>
		<?php  
		$no++;
	    endforeach;
		?>
	</table>
	<center><button style="width: 180px; height: 70px; margin-top: 50px;" class="buttonSubmit">
		<a href="tambahdatamobil.php" style="text-decoration: none; font-size: 15px; font-weight: bold;">Tambah Data</a></button></center>
		<center><button style="width: 180px; height: 70px; margin-top: 50px;" class="buttonSubmit">
		<a href="index.html" style="text-decoration: none; font-size: 15px; font-weight: bold;">Beranda</a></button></center>
</body>
</html>
