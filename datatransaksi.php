<?php  
require_once("confiq.php");
$sql_get = "SELECT * FROM transaksi";
$query_tran = mysqli_query($koneksi, $sql_get);

$results = [];
while ($row = mysqli_fetch_assoc($query_tran)) {
	$results[] = $row;
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Data History Mobil</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<style type="text/css">
		.tabeldatatransaksi{
			margin: auto;
			font-weight: bold;
			color: black;
			background-color: white;
			margin-top: 100px;
		}
		.datatransaksi{
			text-align: center;
			background-image: url(img/bgbgbg.png);
		}
		.buttonSubmit{
			border-radius: 25px;
			background-color: green;
		}
		.buttonSubmit a{
			color: white;
		}
		table{
			border-collapse: collapse;
		}
		table td {
			border:1px solid; padding: 6px;
		}
	</style>
</head>
<body class="datatransaksi">
	<table class="tabeldatatransaksi" border="">
		<tr>
			<td>No</td>
			<td>Kode Transaksi</td>
			<td>Tanggal Kembali</td>
			<td>Lama Sewa</td>
			<td>Harga Total</td>
			<td>Status</td>
			<td>Sewa Berapa</td>
			<td>Kode Mobil</td>
			<td>Kode Pembeli</td>
			<td>Aksi</td>
		</tr>
		<?php  
		$no = 1;
		foreach ($results as $dataterbaca) :
		?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $dataterbaca['kode_transaksi'] ?></td>
			<td><?= $dataterbaca['tgl_kembali'] ?></td>
			<td><?= $dataterbaca['lama_sewa'] ?></td>
			<td><?= $dataterbaca['harga_total'] ?></td>
			<td><?= $dataterbaca['status'] ?></td>
			<td><?= $dataterbaca['sewa_brp'] ?></td>
			<td><?= $dataterbaca['kode_as'] ?></td>
			<td><?= $dataterbaca['kode_p'] ?></td>
			<td>
				<a href="hapusdatatransaksi.php?kode_transaksi=<?= $dataterbaca['kode_transaksi']; ?>">Hapus</a>
				||
				<a href="ubahdatatransaksi.php?kode_transaksi=<?= $dataterbaca['kode_transaksi']; ?>">Edit</a>
			</td>
		</tr>
		<?php  
		$no++;
	    endforeach;
		?>
	</table>
	<center><button style="width: 180px; height: 70px; margin-top: 50px; " class="buttonSubmit">
		<a href="tambahdatatransaksi.php" style="text-decoration: none; font-size: 15px; font-weight: bold;">Pemesanan Mobil</a></button></center>
		<center><button style="width: 180px; height: 70px; margin-top: 50px;" class="buttonSubmit">
		<a href="index.html" style="text-decoration: none; font-size: 15px; font-weight: bold;">Beranda</a></button></center>
</body>
</html>
